<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

if ( ! function_exists('lang_url'))
{
	/**
	 * Localized Site URL
	 *
	 * @param	string
	 * @return	string
	 */
	function lang_url($uri = '')
	{
		$CI =& get_instance();
		return $CI->config->lang_url($uri);
	}
}
