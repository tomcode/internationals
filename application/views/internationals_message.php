<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<title>Welcome to CodeIgniter</title>

	<style type="text/css">

	::selection{ background-color: #E13300; color: white; }
	::moz-selection{ background-color: #E13300; color: white; }
	::webkit-selection{ background-color: #E13300; color: white; }

	body {
		background-color: #fff;
		margin: 40px;
		font: 13px/20px normal Helvetica, Arial, sans-serif;
		color: #4F5155;
	}

	a {
		color: #003399;
		background-color: transparent;
		font-weight: normal;
	}

	h1 {
		color: #444;
		background-color: transparent;
		border-bottom: 1px solid #D0D0D0;
		font-size: 19px;
		font-weight: normal;
		margin: 0 0 14px 0;
		padding: 14px 15px 10px 15px;
	}

	code {
		font-family: Consolas, Monaco, Courier New, Courier, monospace;
		font-size: 12px;
		background-color: #f9f9f9;
		border: 1px solid #D0D0D0;
		color: #002166;
		display: block;
		margin: 14px 0 14px 0;
		padding: 12px 10px 12px 10px;
	}

	#body{
		margin: 0 15px 0 15px;
	}
	
	p.footer{
		text-align: right;
		font-size: 11px;
		border-top: 1px solid #D0D0D0;
		line-height: 32px;
		padding: 0 10px 0 10px;
		margin: 20px 0 0 0;
	}
	
	#container{
		margin: 10px;
		border: 1px solid #D0D0D0;
		-webkit-box-shadow: 0 0 8px #D0D0D0;
	}
	</style>
</head>
<body>

<?php

	$default_controller = $this->router->default_controller;
	
	$cur_controller = $controller_seg = $this->uri->rsegment(1);

	$controller_seg = $controller_seg === $default_controller ? '' : '/' .$controller_seg;
	
	
	$cur_lang_segment = $this->config->cur_lang_segment(true);
	
	$is_foreign_mode = $this->config->item('foreign_mode');

	$is_foreign_default = $is_foreign_mode && $this->config->is_default_language();
	
	$languages = $this->config->item('languages');
	
	$lang_segments = array_flip($languages);
	
	$default_lang_segment = $this->config->default_lang_segment();
	
?>
<div id="container">
	<h1>Welcome to Internationals for CodeIgniter!</h1>

	<div id="body">
		<p>Your current controller is <?php echo ucfirst($cur_controller)?>.<p>
		<p>Your current language segment is <?php echo $cur_lang_segment?>.
<?php

	if($is_foreign_default) :
	
?>
		Since Your're using foreign mode and <?php echo $languages[$cur_lang_segment]?> is
		Your default language, the segment is not shown in the URL's.
<?php

	else :

		echo '<br><br>';
		
	endif;
		
	?></p>
		<ul>
<?php

	foreach($lang_segments as $lang_segment) :
	
		$anchor_text = $lang_segment;
		
		if($is_foreign_mode && $default_lang_segment === $lang_segment) :
		
			$lang_segment = '';
		
		endif;
?>
			<li><?php echo anchor($lang_segment .$controller_seg, $anchor_text)?></li>
<?php

	endforeach;
	
?>
		</ul>
		<p><a href="<?php echo lang_url('another')?>">anoher controller</p>
	</div>

	<p class="footer">Page rendered in <strong>{elapsed_time}</strong> seconds</p>
</div>

</body>
</html>