<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Another extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		
		// required for redirects
		// and view internationals_message
		$this->load->helper('url');
		
		$this->init_internationals();
	}
	
	// --------------------------------------------------------------------

	public function index()
	{
		$this->load->view('internationals_message');
	}

	// --------------------------------------------------------------------

	protected function init_internationals()
	{
		$this->load->helper('internationals');
		$this->load->config('internationals');
		
		$lang_segment = $this->uri->segment(1);
		
		// check only if not in foreign mode
		if( ! $this->config->item('foreign_mode'))
		{
			// we will always have a language segment
			if( ! $lang_segment)
			{
				redirect($this->config->default_lang_segment());
			}
			
			// and it must be a valid laguage segment
			$valid_lang_segment = $this->config->item($lang_segment, 'languages');
			
			if( ! $valid_lang_segment)
			{
				show_404();
			}
		}
		
		$this->config->set_language($lang_segment);
	}
}
