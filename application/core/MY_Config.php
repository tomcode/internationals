<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * CodeIgniter
 *
 * An open source application development framework for PHP 4.3.2 or newer
 *
 * @package		CodeIgniter
 * @author		ExpressionEngine Dev Team
 * @copyright	Copyright (c) 2008 - 2009, EllisLab, Inc.
 * @license		http://codeigniter.com/user_guide/license.html
 * @link		http://codeigniter.com
 * @since		Version 1.0
 * @filesource
 */

// ------------------------------------------------------------------------

/**
 * Extension for the CodeIgniter Config Class
 *
 * Localizes site URLs, english gets the en segment
 * prepended.
 *
 * @package		CodeIgniter
 * @subpackage	Libraries
 * @category	Libraries
 * @author		Thomas Traub
 * @link		http://thomastraub.com/
 */
class MY_Config extends CI_Config {
	
	/**
	 * Constructor
	 *
	 * Add the config variable default_language
	 */
	public function __construct()
	{
		parent::__construct();
		
		if( ! $this->check_enabled())
		{
			log_message('error', 'MY_Config internationals item languages is missing');

			show_error('Wrong setup for internationals');
		}
		else
		{
			$this->set_default_language($this->config['language']);

			log_message('debug', "MY_Config Class Initialized");
		}
	}
  	
	// --------------------------------------------------------------------

	private function check_enabled()
	{
		$languages = $this->item('languages');

		return ! is_array($languages) || ! count($languages);
	}
		
	// --------------------------------------------------------------------

	public function set_language($iso_lang)
	{
		$languages = $this->item('languages');
		
		$language_keys = array_flip($languages);
		
		if(in_array($iso_lang, $language_keys))
		{
			$this->set_item('language', $languages[$iso_lang]);
		}
	}
		
	// --------------------------------------------------------------------

	/**
	 * Tells whether the current language is the default langugage
	 *
	 * @return	boolean
	 */
	public function is_default_language()
	{
		return $this->config['language'] === $this->default_language();
	}

	// --------------------------------------------------------------------

	/**
	 * Returns the default langugage
	 *
	 * @return	string
	 */
	public function default_language()
	{
		return $this->config['default_language'];
	}

	// --------------------------------------------------------------------

	/**
	 * Returns the default language segment
	 *
	 * @return	string
	 */
	public function default_lang_segment()
	{
		$languages = $this->item('languages');
		$segments = array_flip($languages);
		$d_l = $this->config['default_language'];
		
		if(isset($segments[$d_l]))
		{
			return $segments[$d_l];
		}
		
		return $segments[0];
	}

	// --------------------------------------------------------------------

	/**
	 * Tells whether the current language is the default langugage
	 *
	 * @return	string
	 */
	public function set_default_language($lang)
	{
		$this->config['default_language'] = $lang;
	}

	// --------------------------------------------------------------------

	/**
	 * Returns the ISO 639.1 idents with CI idents as keys
	 *
	 * @return	array
	 */
	public function iso639_languages($foreigns_only = false)
	{
		$languages = array_flip($this->item('languages'));

		if($foreigns_only)
		{
			unset($languages[$this->item('default_language')]);
		}
		
		return $languages;
	}

	// --------------------------------------------------------------------

	/**
	 * Returns the CI language ident for a ISO 639.1 ident
	 *
	 * Only for installed languages
	 *
	 * @return	mixed string|boolean
	 */
	public function language_key($iso639_key)
	{
		$languages = $this->item('languages');
		
		if(isset($languages[$iso639_key]))
		{
			return $languages[$iso639_key];
		}
		
		return false;
	}

	// --------------------------------------------------------------------

	/**
	 * Returns the ISO 639.1 ident for a CI language ident
	 *
	 * Only for installed languages
	 *
	 * @return	mixed string|boolean
	 */
	public function iso639_key($ci_language)
	{
		$languages = $this->iso639_languages();
		
		if(isset($languages[$ci_language]))
		{
			return $languages[$ci_language];
		}
		
		return false;
	}

	// --------------------------------------------------------------------

	/**
	 * Whether the passed segment array contains a language segment
	 *
	 * Works only for installed languages
	 *
	 * @return	boolean
	 */
	public function is_localized($segment_array)
	{
		reset($segment_array);
		
		return in_array(current($segment_array), array_flip($this->item('languages')));
	}

	// --------------------------------------------------------------------

	/**
	 * Returns current lang segment
	 *
	 * @param	boolean optional return also default when in foreign mode
	 * @return	mixed string|boolean
	 */
	public function cur_lang_segment($return_always = false)
	{
		if(! (! $return_always && $this->item('foreign_mode') && $this->is_default_language()))
		{
			return $this->iso639_key($this->item('language'));
			
		}
		else return '';
	}

	// --------------------------------------------------------------------

	/**
	 * Returns the language segment of the passed uri_string
	 *
	 * @param	mixed string|array the URI string
	 * @return	mixed string|boolean 
	 */
	public function lang_segment($uri = '')
	{
		if (is_string($uri))
		{
			$uri = explode('/', $uri);
		}
		reset($uri);
		
		$segment = current($uri);
		
		return in_array($segment, $this->item('languages')) ? $segment : false;
	}

	// --------------------------------------------------------------------

	/**
	 * Prepends a language
	 *
	 * @param	mixed string|array the URI string
	 * @return	mixed string|array the localized URI string
	 */
	public function lang_uri($uri = '', $lang_segment)
	{
		if($is_string = is_string($uri))
		{
			$uri = explode('/', $uri);
		}

		if(! $this->is_localized($uri))
		{
			if($uri[0])
			{
				array_unshift($uri, $lang_segment);
			}
			else $uri[0] = $lang_segment;
		}
		
		return $is_string ? implode('/', $uri) : $uri;
	}

	// --------------------------------------------------------------------

	/**
	 * Localized Site URL
	 *
	 * @param	mixed string|array the URI string
	 * @return	string
	 */
	public function lang_url($uri = '')
	{
		if($is_string = is_string($uri))
		{
			$uri = explode('/', $uri);
		}

		if(! $this->is_localized($uri))
		{
			if($lang_segment = $this->cur_lang_segment())
			{
				array_unshift($uri, $lang_segment);
			}
		}
		
		return $this->site_url($uri);
	}

}
