<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/*
|--------------------------------------------------------------------------
| Default Language
|--------------------------------------------------------------------------
|
| The default language is dynamically set. It will contain
| the config item language of the main config file config.php.
|
| Do not uncomment it. I've added it to document that the name is used.
|
*/
// $config['default_language'] = 'english';

/*
|--------------------------------------------------------------------------
| Languages URI Handling : Foreign Mode
|--------------------------------------------------------------------------
|
| Triggers URI behaviour for the helper function lang_url()
|
| - set to true will create localized URI's EXCEPT for the default language,
| - set to false will create localized URI's for all languages,
|
*/
$config['foreign_mode'] = true;

/*
|--------------------------------------------------------------------------
| Languages (ISO 639.1 to CI mapper)
|--------------------------------------------------------------------------
|
| The accepted languages in ISO 639.1 notation (two letters) as keys,
| the corresponding CI language packs are required.
|
| The keys correspond to the first URI segment.
| The values correspond to CI's language names
|
|	array('en' => 'english', 'fr' => 'french')
|
*/
$config['languages'] = array
(
	'en' => 'english',
	'fr' => 'french',
	'de' => 'german'
);
