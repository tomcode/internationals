<?php  if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * User Agent Class Extension
 *
 * Matches the language of the browsing agent 
 *
 * @package		CodeIgniter
 * @subpackage	Libraries
 * @category	User Agent
 * @author		Thomas Traub
 */
class MY_User_agent extends CI_User_agent {
	
	
	/**
	 * Matches a list of languages to the agent languages 
	 *
	 * @param array numeric ISO 639.1 (two letter) language idents
	 * @return mixed the best match or false
	 */
	public function match_language($languages)
	{
		$browser_langs = $this->languages();
	
		$lang_match = false;
		
		foreach($browser_langs as $browser_lang)
		{
			$browser_lang = $this->iso_639_1($browser_lang);
		
			if(in_array($browser_lang, $languages))
			{
				$lang_match = $browser_lang;
			
				break;
			}
		}
		
		return $lang_match;
	}
	
	public function iso_639_1($browser_lang)
	{
		$browser_lang_parts = explode('-', trim($browser_lang, '-'));
		
		return $browser_lang_parts[0];
	}
}
